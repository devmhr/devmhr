-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2017 at 05:25 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `recipe`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `recipe_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `healthy_diet`
--

CREATE TABLE `healthy_diet` (
  `id` int(10) NOT NULL,
  `disease_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `healthy_diet_recipe`
--

CREATE TABLE `healthy_diet_recipe` (
  `recipe_id` int(10) NOT NULL,
  `healthy_diet_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ingredient`
--

CREATE TABLE `ingredient` (
  `id` int(10) NOT NULL,
  `ingredient_name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(10) NOT NULL,
  `rating` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `recipe_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE `recipe` (
  `id` int(10) NOT NULL,
  `recipe_name` varchar(200) DEFAULT NULL,
  `recipe_description` varchar(500) DEFAULT NULL,
  `user_id` int(10) NOT NULL,
  `category` text,
  `status_id` int(10) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ingredient` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `recipe`
--

INSERT INTO `recipe` (`id`, `recipe_name`, `recipe_description`, `user_id`, `category`, `status_id`, `image_url`, `created_at`, `updated_at`, `ingredient`) VALUES
(37, 'Jholay MoMo', 'These popular street dumplings are so versatile. You just need to master the technique and you can twist them to suit your palate.', 2, 'lunch', 1, '1495701971.jpg', '2017-05-25 03:01:12', '2017-05-25 03:01:12', 'For Dough:\r\n2 cups maida\r\n1/2 tsp salt\r\n1/2 tsp baking powder\r\n\r\nFor Filling:\r\n1 cup carrots - grated\r\n1 cup cabbage - grated\r\n1 Tbsp oil\r\n1/2 cup onion - finely chopped\r\n1 tsp garlic - chopped\r\n1 tsp Soya sauce\r\nSalt\r\n1/4 tsp vinegar\r\n1/4 tsp black pepper'),
(38, 'Carrot, Tomato, and Spinach Quinoa Pilaf', '\"I remember my aunt telling me about quinoa, saying it was such a wonderfully healthy food and what do you know? ', 2, 'Breakfast', 1, '1495702747.jpg', '2017-05-25 03:14:07', '2017-05-25 03:14:07', '2 teaspoons olive oil 1/2 onion, chopped 1 cup quinoa 2 cups water 2 tablespoons vegetarian chicken-flavored bouillon granules 1 teaspoon ground black pepper 1 teaspoon thyme 1 carrot, chopped 1 tomato, chopped'),
(40, 'Fried Rice', 'If you are getting bired by eating the plain rice and when you want something interesting with your rice try this..', 2, 'Dinner', 1, '1495706369.jpg', '2017-05-25 04:14:31', '2017-05-25 04:14:31', '3 cups cold leftover cooked rice\r\n1/2-1 cup oleo (or butter)\r\n1 medium onion (chopped)\r\n2 stalks celery (chopped, or just use 3 cloves of garlic and)\r\n2 carrots (diced)\r\n1 cup frozen peas (or a small-medium part of ginger)\r\n3 eggs (beaten, or 6)\r\n1/4-1/2 cup soy sauce (to taste, and chicken msg)'),
(41, 'Home Style Chicken Curry', 'A rustic and authentic quick Indian one-pot packed with tonnes of hot Asian spices and fragrant coriander', 2, 'Lunch', 1, '1495706527.jpg', '2017-05-25 04:17:07', '2017-05-25 04:17:07', '1 large onion\r\n    Onion\r\n\r\n    6 garlic cloves, roughly chopped\r\n    50g ginger\r\n    Ginger , roughly chopped\r\n    4 tbsp vegetable oil\r\n    2 tsp cumin seed\r\n    1 tsp fennel seed\r\n    Fennel seeds\r\n\r\n    5cm cinnamon stick\r\n    1 tsp chilli flakes\r\n    1 tsp garam masala\r\n    Garam masala\r\n\r\n    1 tsp turmeric\r\n    Turmeric\r\n\r\n    1 tsp caster sugar\r\n    400g can chopped tomatoes\r\n    8 chicken thighs, skinned, boneless (about 800g)\r\n    250ml hot chicken stock\r\n    2 tbsp chopped coriander'),
(42, 'Pancake', 'It will be slightly thick and lumpy, but should be well incorporated.', 2, 'Breakfast', 1, '1495706978.png', '2017-05-25 04:24:38', '2017-05-25 04:24:38', '1 1/2 cups all-purpose flour\r\n    3 1/2 tsps baking powder\r\n    1/2 tsp salt\r\n    1/4 cup sugar\r\n    1 1/4 cups whole milk\r\n    1 egg\r\n    3 tbsp butter melted'),
(43, 'Paneer', 'Stir through most of the coriander and garnish with the rest. Serve with basmati rice or naan bread, raita or your favourite chutney.', 2, 'Dinner', 1, '1495707152.jpg', '2017-05-25 04:27:33', '2017-05-25 04:27:33', '2 tbsp sunflower oil\r\n    Sunflower oil\r\n\r\n    225g pack paneer, cut into cubes\r\n    1 head of cauliflower\r\n    Cauliflower\r\n\r\n    , broken into small florets\r\n    2 onions\r\n    Onion\r\n\r\n    , thickly sliced\r\n    2 garlic cloves, crushed\r\n    2 heaped tbsp tikka masala paste\r\n    500g carton passata\r\n    200g frozen peas\r\n    small pack coriander, roughly chopped\r\n    basmati rice or naan breads, to serve\r\n    raita or your favourite chutney, to serve'),
(45, 'Noodles Fry', 'Open the packet of noodles and soak it in warm water just before you heat the pan', 2, 'Lunch', 1, '1495707300.jpg', '2017-05-25 04:30:00', '2017-05-25 04:30:00', 'Vegetable oil- 1 table spoon Salt- as per taste \r\nRed chili powder- ½ tea spoon \r\nNoodles- 1 packet (you can take any noodles either white or brown)\r\nOnion- 1 finely chopped\r\nTomato- 1 finely chopped \r\nGreen chili-1 finely chopped\r\nCilantro- 1 teaspoon finely chopped\r\nWarm water- 1 cup'),
(46, 'Home Made IceCream', 'Place sweetened condensed milk in the fridge to keep cold', 2, 'Desert', 1, '1495707452.jpg', '2017-05-25 04:32:32', '2017-05-25 04:32:32', '14 ounces (1 Can/ 400ml) sweetened condensed milk (fat-free or regular) cold\r\n    2 cups (16oz/450 ml ) whipping cream, cold'),
(47, 'Allu Dum', 'This dish keeps nicely in a warm oven (or, I would imagine, in a crock pot).', 2, 'Dinner', 1, '1495707682.jpg', '2017-05-25 04:36:23', '2017-05-25 04:36:23', '20 baby red potatoes\r\n1 small onion, chopped\r\n1 cup chopped tomatoes\r\n1 tsp grated fresh ginger\r\n3 garlic cloves, crushed\r\n1 tsp cumin powder\r\n1/8 tsp ground clove\r\n1 tsp sugar\r\n1 tsp red chili powder (lanka guro) (this is very hot, adjust according to your tolerance)\r\n1/2 tsp garam masala\r\n1 tsp salt\r\n2 tbsp mustard oil\r\n1/2 tsp cumin seeds\r\n2 bay leaves\r\n1 cinnamon stick\r\n4 cardamom pods\r\n2 cups water'),
(48, 'Chatamari', 'Chatamari is an excellent appetizer of Nepalese cuisine. Learn how to make/prepare Chatamari by following this easy recipe.', 2, 'Lunch', 1, '1495707803.jpg', '2017-05-25 04:38:24', '2017-05-25 04:38:24', '2 cup Long Grain Rice (presoaked)\r\n1 cup Black Dal (presoaked)\r\n1 tbsp Roasted Cumin Seed\r\nSalt to taste\r\n1 cup Water\r\nButter'),
(50, 'Choyela', 'Typical Newari non-vegetatian appetizer, Buff Choila can be cooked in two ways, namely Haku Choila and Mana Choila. Often eaten with Alcohol or with beaten rice, or both.\r\nHaku Choila: meat is roasted. [Haku means black]\r\nMana Choila: meat is boiled.', 2, 'Lunch', 1, '1495708010.jpg', '2017-05-25 04:41:51', '2017-05-25 04:41:51', '1. Buffalo meat, 500 grams\r\n2. Two tablespoon mustard oil\r\n3. Two large red tomatoes\r\n4. One tablespoon garlic paste\r\n5. One tablespoon ginger paste\r\n6. Two red chillies \r\n7. One teaspoon cumin powder\r\n8. One teaspoon Szechwan pepper powder/ Timur\r\n9. Half teaspoon Turmeric\r\n10. Half teaspoon Methi seeds (Fenugreek seeds)'),
(51, 'Chilly Sausage', 'Sausage chilly is the international food', 2, 'lunch', 1, '1495708390.jpg', '2017-05-25 04:48:10', '2017-05-25 04:48:10', '1.sausages 6\r\n2.Onions 2 medium\r\n3.Oil 1 tablespoon\r\n4.Green capsicum 1 large\r\n5.Green chillies chopped 2\r\n6.Dark soy sauce 1 teaspoon\r\n7.Red chilli sauce 1 tablespoon\r\n8.Schezwan chutney 2 tablespoons\r\n9.Fresh coriander sprigs 5-6'),
(52, 'Yomari', 'Yomari is the most typical food in a newari community which is like by everyone', 2, 'Lunch', 1, '1495708572.jpg', '2017-05-25 04:51:14', '2017-05-25 04:51:14', '1.Rice flour\r\n2.Hot water\r\n3.Sesame seed\r\n4.Chaku(Molasses)'),
(53, 'Chowmein', 'A Chinese dish consisting of vegetables, meat, and long, thin noodles, all fried together', 2, 'Lunch', 1, '1495708943.jpg', '2017-05-25 04:57:24', '2017-05-25 04:57:24', '1. 200gm noodles     \r\n2. 2tbsp minced garlic     \r\n3. 1/2 spring onions, sliced lengthwise \r\n4. 10 button mushrooms, sliced lengthwise\r\n5. 1 carrot, sliced lengthwise      \r\n6. 11/2tbsp brown or plain sugar \r\n7. 1tbsp soya sauce\r\n8. 1tbsp vinegar\r\n9. 1/2tsp black pepper\r\n10. 2tbsp vegetable oil'),
(54, 'WaiWai Chatpatye', 'Wai Wai chatptaye is the spicy food loved by the young girls', 2, 'Lunch', 1, '1495709377.jpg', '2017-05-25 05:04:37', '2017-05-25 05:04:37', '1 packet Wai Wai Noodles (Veg/Non-veg) \r\n1 small onion\r\n1 small tomato\r\n 2 green chilles\r\n 2 teaspoon lemon juice\r\n 2 teaspoon olive or refined oil');

-- --------------------------------------------------------

--
-- Table structure for table `recipe_step`
--

CREATE TABLE `recipe_step` (
  `id` int(10) NOT NULL,
  `step no` int(10) DEFAULT NULL,
  `instruction` varchar(500) DEFAULT NULL,
  `prep_time` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `recipe_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(10) NOT NULL,
  `user_role` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(10) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status`, `created_at`, `update_at`) VALUES
(1, 'active', NULL, NULL),
(2, 'passive', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userinfo`
--

CREATE TABLE `userinfo` (
  `id` int(10) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `phone` int(50) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userinfo`
--

INSERT INTO `userinfo` (`id`, `username`, `password`, `email`, `address`, `gender`, `phone`, `image`, `created_at`, `updated_at`, `status_id`) VALUES
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-23 01:46:09', '2017-05-23 01:46:09', 1),
(11, 'devmhr', '$2y$10$O1SHXxAR7f1Hi9rtuJGowOXwgG2PWf3z0qKMIIFD/bMt5j/0U7bj.', 'devendra.maharzan@gmail.com', 'teku', 'male', 123123, 'asdasd', '2017-05-24 07:47:43', '2017-05-24 07:47:43', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKcomment869114` (`user_id`),
  ADD KEY `FKcomment444151` (`recipe_id`);

--
-- Indexes for table `healthy_diet`
--
ALTER TABLE `healthy_diet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `healthy_diet_recipe`
--
ALTER TABLE `healthy_diet_recipe`
  ADD PRIMARY KEY (`recipe_id`,`healthy_diet_id`),
  ADD KEY `FKhealthy_di24823` (`healthy_diet_id`),
  ADD KEY `FKhealthy_di149083` (`recipe_id`);

--
-- Indexes for table `ingredient`
--
ALTER TABLE `ingredient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKrating633703` (`user_id`),
  ADD KEY `FKrating58667` (`recipe_id`);

--
-- Indexes for table `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKrecipe375664` (`status_id`),
  ADD KEY `FKrecipe446003` (`user_id`);

--
-- Indexes for table `recipe_step`
--
ALTER TABLE `recipe_step`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKrecipe_ste137038` (`recipe_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `FKuser890583` (`status_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD KEY `FKuser_role943458` (`user_id`),
  ADD KEY `FKuser_role868982` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `healthy_diet`
--
ALTER TABLE `healthy_diet`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ingredient`
--
ALTER TABLE `ingredient`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `recipe_step`
--
ALTER TABLE `recipe_step`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `userinfo`
--
ALTER TABLE `userinfo`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FKcomment444151` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `FKcomment869114` FOREIGN KEY (`user_id`) REFERENCES `userinfo` (`id`);

--
-- Constraints for table `healthy_diet_recipe`
--
ALTER TABLE `healthy_diet_recipe`
  ADD CONSTRAINT `FKhealthy_di149083` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `FKhealthy_di24823` FOREIGN KEY (`healthy_diet_id`) REFERENCES `healthy_diet` (`id`);

--
-- Constraints for table `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `FKrating58667` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  ADD CONSTRAINT `FKrating633703` FOREIGN KEY (`user_id`) REFERENCES `userinfo` (`id`);

--
-- Constraints for table `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `FKrecipe375664` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `FKrecipe446003` FOREIGN KEY (`user_id`) REFERENCES `userinfo` (`id`);

--
-- Constraints for table `recipe_step`
--
ALTER TABLE `recipe_step`
  ADD CONSTRAINT `FKrecipe_ste137038` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`);

--
-- Constraints for table `userinfo`
--
ALTER TABLE `userinfo`
  ADD CONSTRAINT `FKuser890583` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FKuser_role868982` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `FKuser_role943458` FOREIGN KEY (`user_id`) REFERENCES `userinfo` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
