<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//   return view('main.home');
//
//});
//Route::get('/generate/models', '\\Jimbolino\\Laravel\\ModelBuilder\\ModelGenerator5@start');

Route::get('/', ["uses" => "RecipeController@showHome", "as"=>"home"]);




Route::get('/breakfast', ["uses" => "RecipeController@showBreakfast", "as"=>"breakfast"]);


Route::get('/lunch', ["uses" => "RecipeController@showLunch", "as"=>"lunch"]);

Route::get('/details/{id}', ["uses" => "RecipeController@showContent2", "as"=>"details"]);


//project routes

Route::get('/registration', function () {
    return view('layout.registration');

});

Route::post('/registration', ["uses" => "RecipeController@insertUser", "as"=>"insertuser"]);

Route::post('/showRecipeForm', ["uses" => "RecipeController@insertRecipe", "as"=>"insertrecipe"]);


Route::get('/admindash', ["uses" => "RecipeController@showAdminDash", "as"=>"admindash"]);


Route::post('/validateuser', ["uses" => "RecipeController@Authenticate", "as"=>"validateuser"]);




Route::get('/showRecipeForm', function () {
    return view('backend.addrecipe');

});




Route::get('/viewAllRecipe', ["uses" => "RecipeController@viewAllRecipe", "as"=>"viewallrecipe"]);
Route::get('/viewAllUser', ["uses" => "RecipeController@viewAllUser", "as"=>"viewalluser"]);