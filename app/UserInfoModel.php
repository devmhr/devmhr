<?php

namespace devmhr;

use Illuminate\Database\Eloquent\Model;

class UserInfoModel extends Model
{
    //
    protected $table = "user_info";

    public static function getAllData(){
        $recipes = UserInfoModel::all();

        return $recipes;
    }
}
