<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeStep extends Model {

    /**
     * Generated
     */

    protected $table = 'recipe_step';
    protected $fillable = ['id', 'step no', 'instruction', 'prep_time', 'updated_at', 'recipe_id'];


    public function recipe() {
        return $this->belongsTo(\devmhr\Models\Recipe::class, 'recipe_id', 'id');
    }


}
