<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class HealthyDiet extends Model {

    /**
     * Generated
     */

    protected $table = 'healthy_diet';
    protected $fillable = ['id', 'disease_name', 'updated_at'];


    public function recipes() {
        return $this->belongsToMany(\devmhr\Models\Recipe::class, 'healthy_diet_recipe', 'healthy_diet_id', 'recipe_id');
    }

    public function healthyDietRecipes() {
        return $this->hasMany(\devmhr\Models\HealthyDietRecipe::class, 'healthy_diet_id', 'id');
    }


}
