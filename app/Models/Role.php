<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model {

    /**
     * Generated
     */

    protected $table = 'role';
    protected $fillable = ['id', 'user_role', 'updated_at'];


    public function userinfos() {
        return $this->belongsToMany(\devmhr\Models\Userinfo::class, 'user_role', 'role_id', 'user_id');
    }

    public function userRoles() {
        return $this->hasMany(\devmhr\Models\UserRole::class, 'role_id', 'id');
    }


}
