<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class HealthyDietRecipe extends Model {

    /**
     * Generated
     */

    protected $table = 'healthy_diet_recipe';
    protected $fillable = ['recipe_id', 'healthy_diet_id'];


    public function recipe() {
        return $this->belongsTo(\devmhr\Models\Recipe::class, 'recipe_id', 'id');
    }

    public function healthyDiet() {
        return $this->belongsTo(\devmhr\Models\HealthyDiet::class, 'healthy_diet_id', 'id');
    }


}
