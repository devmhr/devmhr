<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    /**
     * Generated
     */

    protected $table = 'comment';
    protected $fillable = ['id', 'comment', 'user_id', 'recipe_id', 'updated_at'];


    public function recipe() {
        return $this->belongsTo(\devmhr\Models\Recipe::class, 'recipe_id', 'id');
    }

    public function userinfo() {
        return $this->belongsTo(\devmhr\Models\Userinfo::class, 'user_id', 'id');
    }


}
