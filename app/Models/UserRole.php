<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {

    /**
     * Generated
     */

    protected $table = 'user_role';
    protected $fillable = ['user_id', 'role_id'];


    public function role() {
        return $this->belongsTo(\devmhr\Models\Role::class, 'role_id', 'id');
    }

    public function userinfo() {
        return $this->belongsTo(\devmhr\Models\Userinfo::class, 'user_id', 'id');
    }


}
