<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model {

    /**
     * Generated
     */

    protected $table = 'ingredient';
    protected $fillable = ['id', 'ingredient_name', 'updated_at'];



}
