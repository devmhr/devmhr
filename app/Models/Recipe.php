<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model {

    /**
     * Generated
     */

    protected $table = 'recipe';
    protected $fillable = ['id', 'recipe_name', 'recipe_description', 'user_id', 'category', 'status_id', 'image_url', 'updated_at', 'ingredient'];


    public function status() {
        return $this->belongsTo(\devmhr\Models\Status::class, 'status_id', 'id');
    }

    public function userinfo() {
        return $this->belongsTo(\devmhr\Models\Userinfo::class, 'user_id', 'id');
    }



    public function healthyDiets() {
        return $this->belongsToMany(\devmhr\Models\HealthyDiet::class, 'healthy_diet_recipe', 'recipe_id', 'healthy_diet_id');
    }


    public function comments() {
        return $this->hasMany(\devmhr\Models\Comment::class, 'recipe_id', 'id');
    }

    public function healthyDietRecipes() {
        return $this->hasMany(\devmhr\Models\HealthyDietRecipe::class, 'recipe_id', 'id');
    }

    public function ratings() {
        return $this->hasMany(\devmhr\Models\Rating::class, 'recipe_id', 'id');
    }

    public function recipeSteps() {
        return $this->hasMany(\devmhr\Models\RecipeStep::class, 'recipe_id', 'id');
    }


}
