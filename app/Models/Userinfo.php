<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model {

    /**
     * Generated
     */

    protected $table = 'userinfo';
    protected $fillable = ['id', 'username', 'password', 'email', 'address', 'gender', 'phone', 'image', 'updated_at', 'status_id'];


    public function status() {
        return $this->belongsTo(\devmhr\Models\Status::class, 'status_id', 'id');
    }



    public function roles() {
        return $this->belongsToMany(\devmhr\Models\Role::class, 'user_role', 'user_id', 'role_id');
    }

    public function comments() {
        return $this->hasMany(\devmhr\Models\Comment::class, 'user_id', 'id');
    }

    public function ratings() {
        return $this->hasMany(\devmhr\Models\Rating::class, 'user_id', 'id');
    }

    public function recipes() {
        return $this->hasMany(\devmhr\Models\Recipe::class, 'user_id', 'id');
    }

    public function userRoles() {
        return $this->hasMany(\devmhr\Models\UserRole::class, 'user_id', 'id');
    }


}
