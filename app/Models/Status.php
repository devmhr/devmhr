<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    /**
     * Generated
     */

    protected $table = 'status';
    protected $fillable = ['id', 'status', 'updated_at'];


    public function recipes() {
        return $this->hasMany(\devmhr\Models\Recipe::class, 'status_id', 'id');
    }

    public function userinfos() {
        return $this->hasMany(\devmhr\Models\Userinfo::class, 'status_id', 'id');
    }


}
