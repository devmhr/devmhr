<?php namespace devmhr\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model {

    /**
     * Generated
     */

    protected $table = 'rating';
    protected $fillable = ['id', 'rating', 'updated_at', 'user_id', 'recipe_id'];


    public function recipe() {
        return $this->belongsTo(\devmhr\Models\Recipe::class, 'recipe_id', 'id');
    }

    public function userinfo() {
        return $this->belongsTo(\devmhr\Models\Userinfo::class, 'user_id', 'id');
    }


}
