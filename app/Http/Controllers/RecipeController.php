<?php

namespace devmhr\Http\Controllers;





use devmhr\Models\Recipe;
use devmhr\Models\Userinfo;

use devmhr\UserInfoModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class RecipeController extends Controller
{
    //


//        $recipes = UserInfoModel::all();
//            $userInfoObj = UserInfoModel::getAllData();

//        print_r($results);die();

    public function showHome()
    {

        try {

            $result = recipe::all();
//            print_r($recipes); die();
//
            if (count($result) == 0)
            {

                return view('main.home')->with(['message' => 'Database is empty']);
            }
            else {

                return view ('main.home')->with(['recipes' => $result]);
            }
        } catch
        (\Exception $e) {
            return $e;
        }


    }

    public function showBreakfast()
    {
        return view('contents.breakfast')->with(['recipes' => '']);
    }

    public function showLunch()
    {
        return view('contents.lunch')->with(['recipes' => '']);
    }

    public function showDinner()
    {
        return view('contents.dinner')->with(['recipes' => '']);
    }

    public function showDessert()
    {
        return view('contents.dessert')->with(['recipes' => '']);
    }


    public function showContent2($id)
    {
        try {

           $result = recipe::find($id);
            //print_r($result); die();

            if (count($result) == 0)
            {

                return view('contents.details')->with(['message' => 'Nothing to show']);
            }
            else {

                return view('contents.details')->with(['recipes' => $result]);
            }
        } catch
        (\Exception $e)
        {
            return $e;
        }
    }

    public function insertUser(Request $req)
    {

        try {
            $userinfo = new Userinfo();
            $userinfo->username = $req->input('username');
            $userinfo->password = bcrypt($req->input('password'));
            $userinfo->email = $req->input('email');
            $userinfo->address = $req->input('address');
            $userinfo->gender = $req->input('gender');
            $userinfo->phone = $req->input('phone');
            $userinfo->image = $req->input('image');
            $userinfo->status_id = 1;
            $userinfo->save();

            return redirect()->route('home')->with(['message' => 'User added']);

        } catch (\Exception $e) {
            return $e;
        }


    }

    /*   public function validateUser(Request $req)
        {
            try{
                $userinfo=new Userinfo();
                if ($userinfo.username==$req->input()

            }
            catch(\Exception $e)
            {
                return $e;
            }
        }*/

    public function insertRecipe(Request $req)
    {

        try {

           /* $image_url=$req->file('image');
            $filename=$image_url->getClientOriginalName();
            Storage::put('/image/recipe'.$filename,file_get_contents($req->file('image')->getRealPath()));*/


            $userinfo = new Recipe();
            $userinfo->recipe_name = $req->input('recipename');
            $userinfo->recipe_description = $req->input('recipedesc');
            $userinfo->user_id = 2;
            $userinfo->category = $req->input('categoryname');
            $userinfo->status_id = 1;
            $userinfo->ingredient = $req->input('ingredient');
           if($req->hasFile('featured_image'))
           {
               $image=$req->file('featured_image');
               $filename=time().'.'.$image->getClientOriginalExtension();
               $location=public_path('images/'.$filename);
               Image::make($image)->resize(800,400)->save($location);

               $userinfo->image_url=$filename;
           }
            $userinfo->save();

            return redirect()->route('admindash')->with(['recipes' => $userinfo]);

        } catch (\Exception $e) {
            return $e;
        }


    }

    public function showAdminDash()
    {
        try {

            $result = recipe::all();
//            print_r($result); die();

            if (count($result) == 0) {

                return view('backend.admindash')->with(['message' => 'Database is empty']);
            } else {

                return view('backend.admindash')->with(['recipes' => $result]);
            }
        } catch
        (\Exception $e) {
            return $e;
        }
    }

    public function viewAllRecipe(Request $req)
    {
        try {

            $result = recipe::all();
//            print_r($result); die();

            if (count($result) == 0) {

                return view('backend.viewrecipe')->with(['message' => 'Database is empty']);
            } else {

                return view('backend.viewrecipe')->with(['recipes' => $result]);
            }
        } catch
        (\Exception $e) {
            return $e;
        }

    }

    public function viewAllUser(Request $req)
    {
        try {

            $result = userinfo::all();
//            print_r($result); die();

            if (count($result) == 0) {

                return view('backend.viewuser')->with(['message' => 'Database is empty']);
            } else {

                return view('backend.viewuser')->with(['users' => $result]);
            }
        } catch
        (\Exception $e) {
            return $e;
        }

    }

    public function Authenticate(Request $req)
    {
        /*$userinfo = new userinfo();


        if ($req->method() == 'POST') {
            /* print_r($username);
             print_r($password);
             die();*/
           /* if ($userinfo->username == $req->input('uname') && $userinfo->password == $req->input('pass'))
            {
                return view('backend.admindash');
            } else
                {
                return redirect()->route('home');
            }


        }*/

        $rules = array(
            'uname'    => 'required', // make sure the email is an actual email
            'pass' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );
        // run the validation rules on the inputs from the form

        $validator=Validator::make(Input::all(),$rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {

            return redirect()->route('home')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('pass')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'username' 	=> Input::get('uname'),
                'password' 	=> Input::get('pass')
            );
//print_r((int)Auth::attempt($userdata));die();
            // attempt to do the login
            if (Auth::attempt($userdata)) {
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
               // console.log('success');
                return redirect()->route('admindash');
            } else {
               // console.log('fail');
                // validation not successful, send back to form
                return redirect()->route('home');
            }
        }
    }

}


