<footer class="footer-distributed">



    <div class="footer-center">
        <p><h3>Lets get in touch!</h3></p>
        <div>
            <i class="fa fa-map-marker"></i>
            <p><span>Kirtipur</span> Nepal</p>
        </div>

        <div>
            <i class="fa fa-phone"></i>
            <p>+977 12223456</p>
        </div>

        <div>
            <i class="fa fa-envelope"></i>
            <p><a href="mailto:support@company.com">mycook@company.com</a></p>
        </div>

    </div>

    <div class="footer-right">

        <p class="footer-company-about">
            <span>About the company</span>
            Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
        </p>

        <div class="footer-icons">

            <a href="http://wwww.facebook.com"><i class="fa fa-facebook"></i></a>
            <a href="http://www.twitter.com"><i class="fa fa-twitter"></i></a>
            <a href="http://"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-github"></i></a>

        </div>

    </div>

</footer>