
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Admin Dashboard</title>
<!-- Bootstrap Core CSS -->
<link href="/new/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="/new/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="/new/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
{{--<link href="/new/morrisjs/morris.css" rel="stylesheet">--}}

<!-- Custom Fonts -->
<link href="/new/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery -->
<script src="/new/jquery/jquery.min.js"></script>

<script src="/js/jquery.js"></script>


<!-- Bootstrap Core JavaScript -->
<script src="/new/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/new/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="/new/raphael/raphael.min.js"></script>
<script src="/new/morrisjs/morris.min.js"></script>
{{--<script src="/new/data/morris-data.js"></script>--}}

<!-- Custom Theme JavaScript -->
<script src="/new/dist/js/sb-admin-2.js"></script>
