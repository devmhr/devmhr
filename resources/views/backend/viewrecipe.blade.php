        <html>
        <head>

            @include('backend.admindeclare')
        </head>



        <div>
            @include('backend.newnavbar')
        </div>

            <body>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">


                 <h1>Recipe Details</h1>

        <table border="2">
            <thead>
            <tr>
                <th>Recipe Name</th>
                <th>Recipe Description</th>
                <th>User id</th>
                <th>Category</th>

                <th>Status Id</th>
                <th>Image</th>
                <th>Ingredient</th>
            </tr>
            </thead>

            @if(isset($recipes))
                @foreach($recipes as  $recipe)
                    <tbody>
                    <tr>
                        <td>{{$recipe->recipe_name}}</td>
                        <td>{{$recipe->recipe_description}}</td>
                        <td>{{$recipe->user_id}}</td>
                        <td>{{$recipe->category}}</td>
                        <td>{{$recipe->status_id}}</td>
                        <td><img src="{{asset('images/'.$recipe->image_url)}}" height="200px" width="200px"></td>
                        <td>{{$recipe->ingredient}}</td>
                    </tr></tbody>
                @endforeach
            @endif


        </table>

                </div>


        </div>
        </div>

            </body>

        </html>









