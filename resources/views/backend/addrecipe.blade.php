<html>
<head>
    @include('backend.admindeclare')
</head>
<body>


<div>
    @include('backend.newnavbar')
</div>


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add category</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <!-- /.panel-heading -->

                {{Form::open(['url'=>'/showRecipeForm','class'=>'form-group','method' => 'POST','files'=>'true'])}}

                    <div class="form-group">
                        {{Form::file('featured_image')}}
                    </div>


                    <div class="form-group">
                        {{Form::text('recipename','',['class'=>'form-control','placeholder'=>'Enter your recipe name'])}}
                    </div>

                    <div class="form-group">

                        <div class="form-group">
                            {{Form::textarea('recipedesc','',['class'=>'form-control','placeholder'=>'Enter your recipe description'])}}
                        </div>

                        <div class="form-group">
                            {{Form::text('categoryname','',['class'=>'form-control','placeholder'=>'Enter your category name'])}}
                        </div>

                        <div class="form-group">
                            {{Form::textarea('ingredient','',['class'=>'form-control','placeholder'=>'Enter your ingredients'])}}
                        </div>

                        {{Form::submit('Add recipe',['class'=>'btn btn-primary'])}}
                        {{ csrf_field() }}

                        {{Form::close()}}


            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>

</div>


</body>
</html>