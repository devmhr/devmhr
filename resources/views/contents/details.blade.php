@include('layout.declare')
<body>


<div class="container-fixed">

    @include('layout.navbar')

</div>
<div class="container">
    <div class="row">
        <div class="col-md-6">

                @if(isset($recipes))
                    {{--@foreach($recipes as  $recipe)--}}
                    {{--{{dd($recipe)}}--}}
            <p> {{$recipes->recipe_name}}
            {{$recipes->recipe_description}}
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star"></span>
            <span class="glyphicon glyphicon-star-empty"></span>
            </p>
            {{--@endforeach--}}
            @endif

        </div>
        <div class="col-md-6">



            <img src="{{asset('images/'.$recipes->image_url)}}" width="550px" height="240px">

        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-4">
            Ingredient
            <ul>
                <li>1 tea spoon</li>
                <li>1 tea spoon</li>
                <li>1 tea spoon</li>
                <li>1 tea spoon</li>
        </div>


        <div class="col-sm-4">
            <div class="spacer" style="height:20px;"></div>

            <li>1 tea spoon</li>
            <li>1 tea spoon</li>
            <li>1 tea spoon</li>
            <li>1 tea spoon</li>
            </ul>
        </div>
    </div>
    <div class="well">
        <u>Direction</u><span class="text pull-right">Add new note</span><span class="glyphicon glyphicon-pencil pull-right"></span>
        <ol type="1">
            <li><em>
                    Bring a large pot of salted water to a boil; cook linguine in boiling water until nearly tender, 6 to 8 minutes. Drain.</li>
            <li>
                Melt 2 tablespoons butter with 2 tablespoons olive oil in a large skillet over medium heat. Cook and stir shallots, garlic, and red pepper flakes in the hot butter and oil until shallots are translucent, 3 to 4 minutes. Season shrimp with kosher salt and black pepper; add to the skillet and cook until pink, stirring occasionally, 2 to 3 minutes. Remove shrimp from skillet and keep warm.</li>

            <li>
                Pour white wine and lemon juice into skillet and bring to a boil while scraping the browned bits of food off of the bottom of the skillet with a wooden spoon. Melt 2 tablespoons butter in skillet, stir 2 tablespoons olive oil into butter mixture, and bring to a simmer. Toss linguine, shrimp, and parsley in the butter mixture until coated; season with salt and black pepper. Drizzle with 1 teaspoon olive oil to serve.

            </li></em>
        </ol></div>

</div></body>

@include('layout.footer')

</body>