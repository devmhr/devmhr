<html>
<head>

    @include('layout.declare')
</head>



<body>


<div class="container-fixed">

    @include('layout.navbar')

</div>

<div class="well">
    <div class="row">
        <div class="col-sm-4 text-center">
            <p class="text-center"><strong>Breakfasts</strong></p><br>
            <a href="#demo1" data-toggle="collapse">
                <img src="/image/conn3.jpg" class="img-circle person" alt="Random Name" width="255" height="255">
            </a>
            <div id="demo1" class="collapse">
                <p>Shrimps</p>
                <p>Kaneki Ken</p>
                <p>The 'One Eyed King'</p>
            </div>
        </div>

        <div class="col-sm-4 text-center">
            <p class="text-center"><strong>Breakfasts</strong></p><br>
            <a href="#demo2" data-toggle="collapse">
                <img src="/image/fish.jpg" class="img-circle person" alt="Random Name" width="255" height="255">
            </a>
            <div id="demo2" class="collapse">
                <p>Salmon</p>
                <p>Kaneki Ken</p>
                <p>The 'One Eyed King'</p>
            </div>
        </div>


        <div class="col-sm-4 text-center">
            <p class="text-center"><strong>Breakfasts</strong></p><br>
            <a href="#demo3" data-toggle="collapse">
                <img src="/image/con2.jpg" class="img-circle person" alt="Random Name" width="255" height="255">
            </a>
            <div id="demo3" class="collapse">
                <p>Fish cury</p>
                <p>DevMhr</p>
                <p>Class 'Warlock'</p>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-4 text-center">
            <p class="text-center"><strong>Breakfasts</strong></p><br>
            <a href="#demo4" data-toggle="collapse">
                <img src="/image/con1.jpg" class="img-circle person" alt="Random Name" width="255" height="255">
            </a>
            <div id="demo4" class="collapse">
                <p>Chicken breasts</p>
                <p>Ulquiorra Schiffer</p>
                <p>'Vasto Lorde' (2<sup>nd</sup> release form)</p>
            </div>
        </div>


        <div class="col-sm-4 text-center">
            <p class="text-center"><strong>Breakfasts</strong></p><br>
            <a href="#demo5" data-toggle="collapse">
                <img src="/image/2.jpg" class="img-circle person" alt="Random Name" width="255" height="255">
            </a>
            <div id="demo5" class="collapse">
                <p>Fishball</p>
                <p>Ulquiorra Schiffer</p>
                <p>'Vasto Lorde' (2<sup>nd</sup> release form)</p>
            </div>
        </div>


    </div>
</div>

<div>

    @include('layout.footer')
</div>
</body>

</html>