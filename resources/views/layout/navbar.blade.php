        <nav class="navbar navbar-default navbar-fixed-top">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">

                <!-- Button that toggles the navbar on and off on small screens -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

                    <!-- Hides information from screen readers -->
                    <span class="sr-only"></span>

                    <!-- Draws 3 bars in navbar button when in small mode -->
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- You'll have to add padding in your image on the top and right of a few pixels (CSS Styling will break the navbar) -->

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <a href="{{ route('home') }}"><img class="img pull-left" src={{asset('image/3.png')}} width="135x" height="42px"></a>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span style="padding:35px">Browse<b class="caret"></b>
                            </span></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <li>Meal type</li>
                                        <li class="divider"></li>
                                        <li><a href="{{route('breakfast')}}">Breakfast</a></li>
                                        <li><a href="">Lunch</a></li>

                                        <li><a href="">Dinner</a></li>
                                        <li><a href="">Dessert</a></li>


                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <li>Ingredient</li>
                                        <li class="divider"></li>
                                        <li><a href="#">Chicken</a></li>
                                        <li><a href="#">Buff</a></li>

                                        <li><a href="#">Salmon</a></li>
                                        <li><a href="#">Pork</a></li>

                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <li>Seasonal</li>

                                        <li class="divider"></li>
                                        <li><a href="#">Birthday</a></li>
                                        <li><a href="#">Mother's day</a></li>

                                        <li><a href="#">Holidays</a></li>
                                        <li><a href="#">More events</a></li>

                                    </ul>
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <li>Meal type</li>
                                        <li class="divider"></li>
                                        <li><a href="breakfast.php">Breakfast</a></li>
                                        <li><a href="lunch.php">Lunch</a></li>

                                        <li><a href="dinner.php">Dinner</a></li>
                                        <li><a href="dessert.php">Dessert</a></li>

                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <li>Ingredient</li>

                                        <li class="divider"></li>
                                        <li><a href="#">Chicken</a></li>
                                        <li><a href="#">Buff</a></li>

                                        <li><a href="#">Salmon</a></li>
                                        <li><a href="#">Pork</a></li>

                                    </ul>
                                </div>
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <li>Seasonal</li>

                                        <li class="divider"></li>
                                        <li><a href="#">Birthday</a></li>
                                        <li><a href="#">Mother's day</a></li>

                                        <li><a href="#">Holidays</a></li>
                                        <li><a href="#">More events</a></li>

                                    </ul>
                                </div>
                            </div>
                        </ul>
                    </li>



                    <!--/.navbar-collapse-->



                    <!-- navbar-left will move the search to the left-->
                    <li>
                        <form class="navbar-form" role="search">

                            <div class="inner-addon left-addon">
                                <i class="glyphicon glyphicon-search"></i>
                                <div class="form-group">

                                    <input type="text" class="form-control" id="iR" placeholder="Search" style="width: 170%" />
                                </div>

                            </div>
                        </form>


                    </li>


                    <li><a href="#"data-toggle="modal" data-target="#myModal" style="position: relative;right: -500px"><span class="glyphicon glyphicon-user"></span>
                            Login</a></li>

                </ul>
            </div>
        </nav>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="col-md-5 col-md-offset-4">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4><span class="glyphicon glyphicon-user"></span> User login</h4>
                    </div>


                    <div class="well">


                            {{Form::open(['action'=>'RecipeController@Authenticate','class'=>'form-group','method' => 'POST'])}}
                        <p>
                            {{ $errors->first('uname') }}
                            {{ $errors->first('pass') }}
                        </p>

                        <div class="form-group">
                            {{Form::text('uname','',['class'=>'form-control','placeholder'=>'Enter your username'])}}
                        </div>


                        <div class="form-group">

                            {{Form::password('pass',['class'=>'form-control','placeholder'=>'Enter your password'])}}</div>
                        {{Form::submit('Login',['class'=>'btn btn-primary'])}}
                        {{Form::close()}}
                        <hr>
                        <center>OR</center><br>
                        <a href="https://www.facebook.com/v2.2/dialog/oauth?client_id=421150811597210%0D%0A&amp;state=5566caf42d56de33a875fd289d42f571&amp;response_type=code&amp;sdk=php-sdk-5.0.0&amp;redirect_uri=http%3A%2F%2Flocalhost%2Ffacebook_login_with_php%2F&amp;scope=email">
                            <img src="/image/fblogin.png" width="215px" height="40px">

                        </a>
                        <img src="/image/gmai2.png" width="215px" height="40px">

                        </button>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Cancel
                        </button>

                        <div><a href="{{route('insertuser')}}">Click here to register</a></div>
                        <p>Need <a href="https://www.google.com">help?</a></p>
                    </div>
                </div>
            </div>
        </div>


