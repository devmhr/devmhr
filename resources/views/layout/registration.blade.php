@include('layout.declare')

<div>
    @include('layout.navbar')

</div>
<div class="container">

    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">




            <div class="panel-heading">Registration Form</div>


            {{Form::open(['url'=>'/registration','class'=>'form-group','method' => 'POST'])}}
            {{--<div class="input-group">
                <span class="input-group-addon"><i class="fa-user"></i></span>
                {{Form::text('name','',['class'=>'form-control','placeholder'=>'Enter your firstname'])}}
                <span class="input-group-btn"><button class="btn btn-primary">test</button> </span>
            </div>--}}
            <div class="form-group">
                {{Form::text('username','',['class'=>'form-control','placeholder'=>'Enter your username'])}}
            </div>


            <div class="form-group">
                {{Form::password('password',['class'=>'form-control','placeholder'=>'Enter your password'])}}</div>

            <div class="form-group">
                {{Form::email('email','',['class'=>'form-control','placeholder'=>'Enter your email'])}}</div>

            <div class="form-group">
                {{Form::text('address','',['class'=>'form-control','placeholder'=>'Enter your address'])}}</div>

            <div class="form-group">
                {{Form::text('gender','',['class'=>'form-control','placeholder'=>'Enter your gender'])}}</div>

            <div class="form-group">
                {{Form::text('phone','',['class'=>'form-control','placeholder'=>'Enter your phone'])}}</div>

            <div class="form-group">
                {{Form::text('image','',['class'=>'form-control','placeholder'=>'Enter your image'])}}</div>

            {{--{{Form::text('status_id','',['class'=>'form-control','placeholder'=>'Enter your status(1 or 2)'])}}<br>--}}

            {{Form::submit('Register',['class'=>'btn btn-primary'])}}

            {{ csrf_field() }}

            {{Form::close()}}

        </div></div>
</div>

<div>
    @include('layout.footer')
</div>