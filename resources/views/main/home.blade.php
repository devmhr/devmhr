
<html>
<head>

@include('layout.declare')
</head>



<body>


<div class="container-fixed">

@include('layout.navbar')

</div>



@include('main.content')


<div>
    @include('layout.footer')
</div>
</body>

</html>