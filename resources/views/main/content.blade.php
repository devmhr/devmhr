
{{--@if (count($errors) > 0)

    <div class="alert alert-danger row">
        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif--}}


<div id="first-slider">
    <div id="carousel-example-generic" class="carousel slide carousel-fade">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <!-- Item 1 -->
            <div class="item active slide1">
                <div class="row"><div class="container">

                        <div class="col-md-9 text-left">
                            <h3>Calorie<br>
                                ->215</h3>
                            <h4>Cooking complexity
                                <br>->Hard</h4>
                            <h4>Cooking time
                                <br>->30min max</h4>
                        </div>
                    </div></div>
            </div>
            <!-- Item 2 -->
            <div class="item slide2">
                <div class="row"><div class="container">
                        <div class="col-md-7 text-left">
                            <h3>Calorie<br>
                                ->65</h3>
                            <h4>Cooking complexity
                                <br>->Easy</h4>
                            <h4>Cooking time
                                <br>->25min max</h4>
                            </h4>

                        </div>


                    </div></div>
            </div>
            <!-- Item 3 -->
            <div class="item slide3">
                <div class="row"><div class="container">
                        <div class="col-md-7 text-left">
                            <h3>Calorie<br>
                                ->165</h3>
                            <h4>Cooking complexity
                                <br>->Medium</h4>
                            <h4>Cooking time
                                <br>->45min max</h4>
                        </div>

                    </div></div>
            </div>
            <!-- Item 4 -->
            <div class="item slide4">
                <div class="row"><div class="container">
                        <div class="col-md-7 text-left">
                            <h3>Calorie<br>
                                ->200</h3>
                            <h4>Cooking complexity
                                <br>->Easy</h4>
                            <h4>Cooking time
                                <br>->1 hr</h4>
                        </div>
                        <
                    </div></div>
            </div>
            <!-- End Item 4 -->


            <!-- End Wrapper for slides-->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
            </a>
        </div>
    </div></div>
        <span class="text-center">
<h4>Welcome! Here are recommendations for you.
</h4><br>
<h5><b>Personalized recommendations based on your tastes and cooks you're following</b></h5>
</span>
<br>


        <div class="container-fluid">

            <div class="row">

                @if(isset($recipes))
                    @foreach($recipes as  $recipe)
                    <div class="col-md-3">


                            <input type="checkbox" id="myCheckbox1" />
                        <label for="myCheckbox1">
                            <a href="{{route('details',['id'=>$recipe->id])}}">

                                <img src="{{asset('images/'.$recipe->image_url)}}" height="260px" width="260px">
                            </a>
                        </label> <p>
                                    {{$recipe->recipe_name}}
                            {{$recipe->recipe_description}}</p>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>

                    </div>
                    @endforeach
                @endif

            </div>
                </div>

